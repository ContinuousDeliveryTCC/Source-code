package br.ufg.pfc.entidades.validators;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.ufg.pfc.entidades.Cliente;
import br.ufg.pfc.entidades.validators.exceptions.ClienteValidatorException;

public class ClienteValidatorTest {

	private Cliente cliente;

	private ClienteValidator sut;

	@Before
	public void init() {
		cliente = new Cliente();
		cliente.setNome("Bruno");
		cliente.setTelefone("62988888888");
		cliente.setEmail("bruno@email.com");

		sut = new ClienteValidator(cliente);
	}

	@Test
	public void deveValidarUmClienteValido() throws ClienteValidatorException {
		sut.validar();
	}

	@Test(expected=ClienteValidatorException.class)
	public void deveInvalidarUmClienteComNomeVazio() throws ClienteValidatorException {
		cliente.setNome(" ");

		sut.validar();
	}

	@Test(expected=ClienteValidatorException.class)
	public void deveInvalidarUmClienteComNomeNulo() throws ClienteValidatorException {
		cliente.setNome(null);

		sut.validar();
	}

	@Test(expected=ClienteValidatorException.class)
	public void deveInvalidarUmClienteComTelefoneVazio() throws ClienteValidatorException {
		cliente.setTelefone("  ");

		sut.validar();
	}

	@Test(expected=ClienteValidatorException.class)
	public void deveInvalidarUmClienteComTelefoneNulo() throws ClienteValidatorException {
		cliente.setTelefone(null);

		sut.validar();
	}

	@Test(expected=ClienteValidatorException.class)
	public void deveInvalidarUmClienteComEmailNulo() throws ClienteValidatorException {
		cliente.setEmail(null);

		sut.validar();
	}

	@Test(expected=ClienteValidatorException.class)
	public void deveInvalidarUmClienteComEmailEmBranco() throws ClienteValidatorException {
		cliente.setEmail("   ");

		sut.validar();
	}
}

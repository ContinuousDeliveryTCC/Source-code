package br.ufg.pfc.entidades.validators;

import static org.slf4j.LoggerFactory.getLogger;

import org.slf4j.Logger;

import br.ufg.pfc.entidades.Cliente;
import br.ufg.pfc.entidades.validators.exceptions.ClienteValidatorException;

public class ClienteValidator {
	
	private static final Logger LOG = getLogger(ClienteValidator.class);

	private Cliente cliente;

	public ClienteValidator(Cliente cliente) {
		this.cliente = cliente;
	}

	public void validar() throws ClienteValidatorException {
		LOG.debug("Validando cliente {}", cliente);
		if (cliente == null) {
			throw new ClienteValidatorException();
		}
		validarNome();
		validarTelefone();
		validarEmail();
	}


	private void validarEmail() throws ClienteValidatorException {
		if(cliente.getEmail() == null
				|| "".equals(cliente.getEmail().trim())) {
			throw new ClienteValidatorException();
		}
	}

	private void validarNome() throws ClienteValidatorException {
		if(cliente.getNome() == null
				|| "".equals(cliente.getNome().trim())) {
			throw new ClienteValidatorException();
		}

	}

	private void validarTelefone() throws ClienteValidatorException {
		if(cliente.getTelefone() == null
				|| "".equals(cliente.getTelefone().trim())) {
			throw new ClienteValidatorException();
		}
	}

}

package br.ufg.pfc.xyzweb;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.pfc.entidades.Cliente;
import br.ufg.pfc.entidades.validators.exceptions.ClienteValidatorException;
import br.ufg.pfc.facade.ClienteFacade;
import br.ufg.pfc.repositorios.exceptions.PersistenciaException;
import br.ufg.pfc.xyzweb.util.FacesUtil;

@Named
@ViewScoped
public class CadastroClienteBean implements Serializable{

	private static final long serialVersionUID = 5589030310130695543L;

	private static final Logger LOG = LoggerFactory.getLogger(CadastroClienteBean.class);

	@Inject ClienteFacade facade;

	private Cliente cliente = new Cliente();

	public void salvar() {
		try {
			LOG.info("Solicitando persistência do cliente");
			facade.cadastrar(cliente);
			LOG.info("Cliente salvo com sucesso");
			FacesUtil.addSuccessMessage("Cliente salvo com sucesso!");
		} catch (ClienteValidatorException | PersistenciaException e) {
			LOG.error("Problema ao salvar o cliente. ", e);
			FacesUtil.addErrorMessage("Problema ao salvar o cliente, " + e.getClass().getName());
		}
	}

	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}


}

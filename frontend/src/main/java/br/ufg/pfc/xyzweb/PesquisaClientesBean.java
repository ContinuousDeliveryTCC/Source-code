package br.ufg.pfc.xyzweb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.ufg.pfc.entidades.Cliente;
import br.ufg.pfc.facade.ClienteFacade;

@Named
@ViewScoped
public class PesquisaClientesBean implements Serializable {

	private static final long serialVersionUID = 6318804275701752536L;

	private List<Cliente> clientes;

	@Inject private ClienteFacade facade;

	@PostConstruct
	public void init() {
		buscarTodos();
	}

	/**
	 * @return the clientes
	 */
	public List<Cliente> getClientes() {
		return clientes;
	}

	/**
	 * Inicia lista de clientes;
	 */
	private void buscarTodos() {
		clientes = facade.pesquisar(new Cliente());
	}

}

CREATE TABLE cliente (
	id SERIAL PRIMARY KEY,
	nome VARCHAR(255) NOT NULL,
	telefone VARCHAR(15),
	email VARCHAR(100) NOT NULL,
	unique (email)
);
package br.ufg.pfc.dao;

public class DBOperationException extends Exception {

	private static final long serialVersionUID = 7063268183891810304L;

	public DBOperationException() {
		super();
	}

	public DBOperationException(String message, Throwable cause) {
		super(message, cause);
	}

	public DBOperationException(String message) {
		super(message);
	}
	
}

package br.ufg.pfc.dao;

import static org.slf4j.LoggerFactory.getLogger;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.slf4j.Logger;

@Named
public class HelloBDDao extends AbstractDao {
	
	private static final long serialVersionUID = -8655140371380255908L;
	private static final Logger LOG = getLogger("Database query test");
	
	@PostConstruct
	public void testDatabaseTime() {
		selectNow();
	}
	
	public void selectNow() {
		LOG.debug("Testando conexão e consulta ao BD");
		try {
			Map<String, Object> now = getTempl().queryForMap("select now()");
			LOG.debug("		Banco retornou hora com o valor  {}", now);
		} catch (Exception e) {
			LOG.error("Erro ao consultar a hora do banco em teste da conexao. Detalhes:", e);
		}
	}
	

}

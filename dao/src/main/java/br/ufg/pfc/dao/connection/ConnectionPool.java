package br.ufg.pfc.dao.connection;

import static org.slf4j.LoggerFactory.getLogger;

import javax.sql.DataSource;

import org.slf4j.Logger;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class ConnectionPool {
	
	private HikariDataSource ds;
	private HikariConfig conf;
	
	private static final Logger LOG = getLogger(ConnectionPool.class);
	
	private static ConnectionPool instance;
	
	public static ConnectionPool getInstanceOf() {
		if (instance == null) {
			LOG.debug("Conexao de BD ainda inexistente. Inicializando...");
			instance = new ConnectionPool();
			instance.start();
			LOG.debug("Conexao de Banco inicializada");
		}
		return instance;
	}
	
	
	public boolean start() {
		LOG.debug("Criando conexão de banco de dados");
		try {
			String userHome = System.getProperty("user.home");
			conf = new HikariConfig(userHome + "/xyz/config/db/db.hikari.properties");
			conf.setConnectionTestQuery("select now()");
			conf.setIdleTimeout(2 * 60 * 60 * 1000);
			conf.setMaxLifetime(10 * 60 * 60 * 1000);
			conf.setPoolName("Pool de conexão");
			LOG.debug("Criada configuração para HikaryCP");			
		} catch (Exception e) {
			LOG.error("Erro ao ler configuração de BD. Detalhes: ", e);
			return false;
		}
		
		try {
			LOG.debug("Configuração adotada: {}", conf );
			ds = new HikariDataSource(conf);
			ds.getConnection();
			LOG.debug("Criado datasource da conexao de BD. Concluido");
			return true;
		} catch (Exception e) {
			LOG.error("Falha ao criar conexao de BD:", e);
			return false;
		}
	}
	
	public boolean stop() {
		LOG.debug("Finalizando conexão de banco");
		ds.close();
		return true;
	}
	
	public DataSource getDatasource() {
		return ds;
	}
	
}

package br.ufg.pfc.dao;

import static org.slf4j.LoggerFactory.getLogger;

import java.io.Serializable;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import br.ufg.pfc.dao.connection.ConnectionPool;

public abstract class AbstractDao implements Serializable{
	
	private static final long serialVersionUID = -5242026300185760876L;
	private static br.ufg.pfc.dao.connection.ConnectionPool connPool;
	private static final Logger LOG = getLogger("Dao generico");
	
	private static ConnectionPool getConnectionPool() {
		return ConnectionPool.getInstanceOf();
	}
	
	private static DataSource getDatasource() {
		return getConnectionPool().getDatasource();
	}
	
	protected JdbcTemplate getTempl() {
		return new JdbcTemplate(getDatasource()); 
	}
	
}

package br.ufg.pfc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import br.ufg.pfc.entidades.Cliente;

/** 
 * Classe de acesso de dados para cliente.
 *
 */
public class ClienteDao extends AbstractDao  {
	
	private static final long serialVersionUID = 665248254065439029L;
	
	private static final Logger LOG = LoggerFactory.getLogger(AbstractDao.class);
	
	public void merge(Cliente cliente) throws DBOperationException {
		String sql = "insert into cliente (nome, email, telefone)"
				+ "values (?,?,?)";
		
		try {
			Integer affectedRows = getTempl().update(sql, new Object[]{
					cliente.getNome(),
					cliente.getEmail(),
					cliente.getTelefone()
			});
			LOG.debug("{} registro de cliente inserido", affectedRows);
		} catch (DataAccessException e) {
			throw new DBOperationException("Erro ao salvar novo cliente no BD", e);
		}
		
	}

	/*
	 * Busca o cliente por email. O Email &eacute; &uacute;nico, portanto retorna apenas a inst&acirc;ncia que tem o email do parâmetro
	 *
	 * @param emailCliente em formato String
	 * @return cliente que possui aquele email, ou nulo caso não haja nenum cliente com esse email cadastrado;
	 */
	public Cliente findByEmail(String emailCliente) {
		
		try {
			List<Cliente> clientes = getTempl().query("select id, nome, email, telefone from cliente where email=?" 
					, new Object[]{emailCliente}, 
					new RowMapper<Cliente>() {
						@Override public Cliente mapRow(ResultSet rs, int rowNum) throws SQLException {
							Cliente cli = new Cliente();
							cli.setId(rs.getLong("id"));
							cli.setEmail(rs.getString("email"));
							cli.setNome(rs.getString("nome"));
							cli.setTelefone(rs.getString("telefone"));
							return cli;
						}
					});
			
			if (clientes != null && !clientes.isEmpty()) {
				return clientes.get(0);
			} else {
				return null;
			}
			
		} catch (DataAccessException e) {
			LOG.error("Erro ao buscar cliente pelo email {}. Considerando resultado vazio", emailCliente);
			LOG.error("Detalhes: ", e);
			return null;
		}
		
	}
	
	/*
	 * Busca todos os clientes 
	 *
	 * @return cliente que possui aquele email, ou nulo caso não haja nenum cliente com esse email cadastrado;
	 */
	public List<Cliente> findAll() {
		
		try {
			List<Cliente> clientes = getTempl().query("select id, nome, email, telefone from cliente" 
					, new Object[]{}, 
					new RowMapper<Cliente>() {
						@Override public Cliente mapRow(ResultSet rs, int rowNum) throws SQLException {
							Cliente cli = new Cliente();
							cli.setId(rs.getLong("id"));
							cli.setEmail(rs.getString("email"));
							cli.setNome(rs.getString("nome"));
							cli.setTelefone(rs.getString("telefone"));
							return cli;
						}
					});
			return clientes;
			
		} catch (DataAccessException e) {
			LOG.error("Detalhes: ", e);
			return null;
		}
		
	}

}

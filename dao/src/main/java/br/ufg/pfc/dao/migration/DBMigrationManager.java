package br.ufg.pfc.dao.migration;

import static org.slf4j.LoggerFactory.getLogger;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.flywaydb.core.Flyway;
import org.omnifaces.cdi.Eager;
import org.slf4j.Logger;

import br.ufg.pfc.dao.connection.ConnectionPool;
import ch.qos.logback.core.joran.spi.NoAutoStart;

@ApplicationScoped
@Named
@Eager
public class DBMigrationManager {
	
	private static ConnectionPool connPool;
	private static final Logger LOG = getLogger(DBMigrationManager.class);
	

	@PostConstruct
	public static void migrate() {
		connPool = ConnectionPool.getInstanceOf();
		Flyway flyway = new Flyway();
		flyway.setDataSource(connPool.getDatasource());
		try {
			LOG.debug("Iniciando migracao de BD");
			flyway.migrate();
			LOG.debug("Migracao de BD concluida");
		} catch (Exception e) {
			LOG.debug("Erro ao executar migracao de BD. Detalhes: ", e);
		}
	}
	
}

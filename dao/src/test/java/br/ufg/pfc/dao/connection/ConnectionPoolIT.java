package br.ufg.pfc.dao.connection;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class ConnectionPoolIT {
	
	ConnectionPool sut;
	
	public static void main(String[] args) {
		ConnectionPool	sut = new ConnectionPool();
		
	}
	
	@Before
	public void init() {
		sut = new ConnectionPool();
	}
	
	@Test
	public void deveConcluirStartup() {
		assertTrue(sut.start());
		sut.stop();
	}
	
	@Test
	public void deveDestruirConexao() {
		sut.start();
		assertTrue(sut.stop());
	}
	

}

package br.ufg.pfc.dao;

import static java.lang.Math.random;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.ufg.pfc.entidades.Cliente;

public class ClienteDaoIT {
	
	ClienteDao sut;
	Cliente cli;
	
	@Before
	public void init() {
		sut = new ClienteDao();
		cli = new Cliente();
		cli.setEmail("email" + random() + "ClienteDaoIt@teste.com");
		cli.setNome("Cliente de teste");
		cli.setTelefone("+5562999856321");
	}

	@Test
	public void deveInserirUmNovoCliente() throws DBOperationException {
		 sut.merge(cli);
	}
	
	
	@Test(expected=DBOperationException.class)
	public void deveFalharAoInserirClienteComEmailNulo() throws DBOperationException {
		cli.setEmail(null);
		sut.merge(cli);
	}
	
	
	@Test(expected=DBOperationException.class)
	public void deveFalharAoInserirClienteComNomeNulo() throws DBOperationException {
		cli.setNome(null);
		sut.merge(cli);
	}
	
	@Test
	public void deveEncontrarClienteExistente() throws DBOperationException {
		 sut.merge(cli);
		 assertNotNull(sut.findByEmail(cli.getEmail()));
	}
	
	@Test
	public void deveRetornarNuloParaClienteInexistente() throws DBOperationException {
		 sut.merge(cli);
		 assertNull(sut.findByEmail("macacosprego@##.com"));
	}

}

package br.ufg.pfc.negocio;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.pfc.entidades.Cliente;
import br.ufg.pfc.entidades.validators.ClienteValidator;
import br.ufg.pfc.entidades.validators.exceptions.ClienteValidatorException;
import br.ufg.pfc.repositorios.IRepositorio;
import br.ufg.pfc.repositorios.exceptions.PersistenciaException;

public class ClienteBusiness implements Serializable{

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = LoggerFactory.getLogger(ClienteBusiness.class);

	@Inject private IRepositorio<Cliente> repoCliente;

	public ClienteBusiness() {
	}

	public ClienteBusiness(IRepositorio<Cliente> repoCliente) {
		this.repoCliente = repoCliente;
	}

	public void cadastrar(Cliente cliente) throws ClienteValidatorException, PersistenciaException {
		try {
			ClienteValidator validator = new ClienteValidator(cliente);
			validator.validar();
			repoCliente.save(cliente);
		} catch (ClienteValidatorException e) {
			LOG.error("Erro ao cadastrar novo cliente:" + e.getLocalizedMessage());
			throw e;
		} catch (PersistenciaException e) {
			LOG.error("Ocorreu um erro ao persistir o cliente.", e);
			throw e;
		}

	}

	public List<Cliente> pesquisar(Cliente cliente) {
		
		List<Cliente> resultado = repoCliente.find(cliente);
		if (resultado == null) {
			resultado = new ArrayList<Cliente>();
		}
		return resultado;
	}

}

package br.ufg.pfc.negocio;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Categories.ExcludeCategory;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.ufg.pfc.entidades.Cliente;
import br.ufg.pfc.entidades.validators.exceptions.ClienteValidatorException;
import br.ufg.pfc.repositorios.IRepositorio;
import br.ufg.pfc.repositorios.exceptions.PersistenciaException;

public class ClienteBusinessTest {

	@Mock private IRepositorio<Cliente> repoMock;
	private ClienteBusiness sut;


	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		sut = new ClienteBusiness(repoMock);
	}


	@Test
	public void deveCadastrarUmClienteValido() throws ClienteValidatorException, PersistenciaException {
		Cliente cliente = new Cliente();
		cliente.setEmail("cliente@email.com");
		cliente.setNome("Teste");
		cliente.setTelefone("33334444");
		sut.cadastrar(cliente);
	}

	@Test(expected=ClienteValidatorException.class)
	public void deveFalharAoCadastrarClienteNulo() throws ClienteValidatorException, PersistenciaException {
		sut.cadastrar(null);
	}


	@Test(expected=ClienteValidatorException.class)
	public void deveInvalidarUmClienteSemNome() throws ClienteValidatorException, PersistenciaException {
		Cliente cliente = new Cliente();
		cliente.setEmail("cliente@email.com");
		cliente.setTelefone("33334444");
		sut.cadastrar(cliente);
	}

	@Test
	public void deveRetornarListaDeClientesNaoNula() {
		Cliente cliente = new Cliente();
		cliente.setNome("joao");

		when(repoMock.find(cliente)).thenReturn(new ArrayList<>());

		assertNotNull(sut.pesquisar(cliente));
	}

}

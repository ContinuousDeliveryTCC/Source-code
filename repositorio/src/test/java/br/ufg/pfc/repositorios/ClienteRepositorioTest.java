package br.ufg.pfc.repositorios;

import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.ufg.pfc.dao.ClienteDao;
import br.ufg.pfc.dao.DBOperationException;
import br.ufg.pfc.entidades.Cliente;
import br.ufg.pfc.repositorios.exceptions.ClienteDuplicadoException;
import br.ufg.pfc.repositorios.exceptions.PersistenciaException;

public class ClienteRepositorioTest {

	@Mock private ClienteDao daoMock;

	private IRepositorio<Cliente> sut;

	private Cliente cliente;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		sut = new ClienteRepositorio(daoMock);

		cliente = new Cliente();
		cliente.setNome("Bruno");
		cliente.setEmail("bruno@email.com");
		cliente.setTelefone("62988888888");
	}

	@Test
	public void devePersitirUmClienteQueNaoTenhaIdAtribuido() throws DBOperationException {
		sut.save(cliente);

		verify(daoMock).merge(cliente);
	}

	@Test(expected=ClienteDuplicadoException.class)
	public void naoDevePersistirUmClienteQueTenhaEmailDuplicadoComOutroClienteNoBanco() throws PersistenciaException {
		when(daoMock.findByEmail(cliente.getEmail())).thenReturn(cliente);

		sut.save(cliente);
	}

}

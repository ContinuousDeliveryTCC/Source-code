package br.ufg.pfc.repositorios.exceptions;

import br.ufg.pfc.dao.DBOperationException;

/**
 * Superclasse de exceção que encapsula as exceptions de persistência gerada pelo repositório
 * @author bruno
 *
 */
public class PersistenciaException extends DBOperationException {

	private static final long serialVersionUID = 10433822408587782L;

	
}

package br.ufg.pfc.repositorios;

import java.io.Serializable;
import java.util.List;

import br.ufg.pfc.entidades.Cliente;
import br.ufg.pfc.entidades.Entidade;
import br.ufg.pfc.repositorios.exceptions.PersistenciaException;

/**
 * Interface que disponibiliza funcionalidades padroes de repositorio
 * @author bruno
 *
 */
public interface IRepositorio<I extends Entidade> extends Serializable {

	/**
	 * M&eacute;todo que disponibiliza funcionalidade de persistir entidade no reposit&oacute;rio
	 *
	 * @param entidade a ser persistida
	 * @throws PersistenciaException caso alguma exce&ccedil;&atilde;o ocorra no momento da persist&ecirc;ncia
	 */
	public void save(I entidade) throws PersistenciaException;

	/**
	 * M&eacute;todo que retorna uma lista de entidades que atendem os par&acirc;metros de busca do objeto filtro recebido;
	 * @param filter objeto entidade que serve como filtro da pesquisa
	 * @return lista de entidades que respondem a esse filtro
	 */
	public List<I> find(I filter);

	/**
	 * Retorna uma lista com todos os itens 
	 * */
	List<I> findAll();

}

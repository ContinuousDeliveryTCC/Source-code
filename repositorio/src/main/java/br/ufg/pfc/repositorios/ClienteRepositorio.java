package br.ufg.pfc.repositorios;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import br.ufg.pfc.dao.ClienteDao;
import br.ufg.pfc.dao.DBOperationException;
import br.ufg.pfc.entidades.Cliente;
import br.ufg.pfc.repositorios.exceptions.ClienteDuplicadoException;
import br.ufg.pfc.repositorios.exceptions.PersistenciaException;

public class ClienteRepositorio implements IRepositorio<Cliente> {

	private static final long serialVersionUID = 1L;

	private ClienteDao clienteDao;

	public ClienteRepositorio() {
		clienteDao = new ClienteDao();
	}

	public ClienteRepositorio(ClienteDao clienteDao) {
		this.clienteDao = clienteDao;
	}

	@Override
	public void save(Cliente entidade) throws PersistenciaException {

		List<Cliente> clientes = find(entidade);

		if(!clientes.isEmpty()) {
			throw new ClienteDuplicadoException();
		}
		
		try {
			clienteDao.merge(entidade);
		} catch (DBOperationException e) {
			throw new PersistenciaException();
		}
	}

	@Override
	public List<Cliente> find(Cliente filter) {
		if (filter!=null && filter.getEmail() !=null ) {
			Cliente cliente = clienteDao.findByEmail(filter.getEmail());
			List<Cliente> list = new ArrayList<Cliente>();
			if (cliente!=null) {
				list.add(cliente);
			}
			return list;
		} else {
			return clienteDao.findAll();
		}
	}
	
	@Override
	public List<Cliente> findAll() {
		return clienteDao.findAll();
	}

}

package br.ufg.pfc.facade;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.ufg.pfc.entidades.Cliente;
import br.ufg.pfc.entidades.validators.exceptions.ClienteValidatorException;
import br.ufg.pfc.negocio.ClienteBusiness;
import br.ufg.pfc.repositorios.exceptions.PersistenciaException;

public class ClienteFacade implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject private ClienteBusiness clienteBusiness;

	public void cadastrar(Cliente cliente) throws ClienteValidatorException, PersistenciaException {
		clienteBusiness.cadastrar(cliente);
	}

	public List<Cliente> pesquisar(Cliente cliente) {
		return clienteBusiness.pesquisar(cliente);
	}

}

